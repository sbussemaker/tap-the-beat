import React from "react";
import { RouteComponentProps, withRouter } from "react-router-dom";
import "./Save.css";
import { Score } from "./Score";
import ScoreStorage from "./ScoreStorage";

interface State {
  name: string;
}
interface Props extends RouteComponentProps {}

class Save extends React.Component<Props, State> {
  scoreStorage: ScoreStorage;
  score: number;
  newHighscore: boolean;

  constructor(props: Props) {
    super(props);
    this.state = { name: "" };

    this.score = this.props.location.state ? this.props.location.state.score : 0;

    this.scoreStorage = ScoreStorage.getInstance();
    const scores: Score[] = this.scoreStorage.getHighscores();
    this.newHighscore = scores.length === 0 || this.score > scores[0].score ? true : false;
  }

  handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ name: event.target.value });
  };

  handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    this.scoreStorage.addScore({
      name: this.state.name,
      time: Date.now(),
      score: this.score
    });

    this.props.history.replace("/");
    event.preventDefault();
  };

  render() {
    return (
      <div className="App">
        <h1>Game over!</h1>

        <section className="Card">
          <p>Your final score was: {this.score}</p>

          {this.newHighscore && <p>New highscore! Well done!</p>}

          <form onSubmit={this.handleSubmit}>
            <label>
              Name: &nbsp;
              <input type="text" value={this.state.name} onChange={this.handleChange} />
            </label>
            <br />
            <input className="SaveButton" type="submit" value="Save" />
          </form>
        </section>
      </div>
    );
  }
}

export default withRouter(Save);
