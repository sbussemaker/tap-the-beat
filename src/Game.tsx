import React from "react";
import { withRouter, RouteComponentProps } from "react-router-dom";

import "./Game.css";

interface State {
  timeLeft: number;
  score: number;
  tapScore: number;
  feedback: string;
}
interface Props extends RouteComponentProps {}

type Result = "PERFECT" | "OKAY" | "MISS";

/**
 * The Game state is where the actual game happens, users have to tap at a certain pace.
 * As it is very hard to EXACTLY tap at that pace, we'll give the user a little slack.
 * Within PERFECT_RANGE milliseconds will still be counted as a perfect hit. Higher values give the user more margin.
 * Same goes for the OKAY score, it will have some range to it as well.
 * The result of a tap, i.e. perfect or okay, is determined by looking at how long ago the last tap was.
 * Using the BPM you know how much time should have passed since the last tap.
 * After time has depleted, redirect to the next page.
 */
class Game extends React.Component<Props, State> {
  readonly MIN_BPM: number = 60;
  readonly MAX_BPM: number = 120;
  readonly GAME_TIME: number = 15; // in seconds. How long a game round lasts
  readonly PERFECT_POINTS: number = 10;
  readonly PERFECT_RANGE: number = 150; // in milliseconds
  readonly OKAY_POINTS: number = 5;
  readonly OKAY_RANGE: number = 500;

  bpm: number;
  timer: number;
  lastTap: number;

  constructor(props: Props) {
    super(props);
    this.state = {
      timeLeft: this.GAME_TIME,
      score: 0,
      tapScore: 0,
      feedback: ""
    };

    this.bpm = Math.round(Math.random() * (this.MAX_BPM - this.MIN_BPM) + this.MIN_BPM); // random bpm
    this.timer = 1;
    this.lastTap = 0;
  }

  componentDidMount() {
    this.timer = setInterval(this.countDown, 1000); // start timer
  }

  componentWillUnmount() {
    clearInterval(this.timer);
  }

  countDown = () => {
    this.setState(state => ({ timeLeft: state.timeLeft - 1 }));

    if (this.state.timeLeft === 0) {
      this.props.history.replace("/save", { score: this.state.score });
    }
  };

  handleTap = () => {
    const now: number = Date.now();

    const timeSpent: number = now - this.lastTap; // in milliseconds
    const mspb: number = 1 / (this.bpm / 60 / 1000); // milliseconds per beat

    // Determine number of points
    let result: Result;
    if (mspb - this.PERFECT_RANGE < timeSpent && timeSpent < mspb + this.PERFECT_RANGE) {
      result = "PERFECT";
    } else if (mspb - this.OKAY_RANGE < timeSpent && timeSpent < mspb + this.OKAY_RANGE) {
      result = "OKAY";
    } else {
      result = "MISS";
    }

    let tapScore: number = 0;
    let feedback: string = "";
    switch (result) {
      case "PERFECT":
        tapScore = this.PERFECT_POINTS;
        feedback = "Perfect!";
        break;
      case "OKAY":
        tapScore = this.OKAY_POINTS;
        feedback = "Okay!";
        break;
      case "MISS":
        tapScore = 0;
        feedback = "Miss!";
        break;
    }

    this.setState(state => ({
      score: state.score + tapScore,
      tapScore,
      feedback
    }));

    this.lastTap = now;
  };

  render() {
    return (
      <div className="App">
        <h1>Tap that Beat</h1>
        <div className="Card">
          <p>
            <span>Goal: </span>Tap {this.bpm} beats per minute
          </p>
          <p>Time left: {this.state.timeLeft} seconds</p>

          <button className="TapButton" onClick={this.handleTap}>
            Tap
          </button>

          <p>Total points: {this.state.score}</p>
          {this.state.feedback && (
            <div id="feedback">
              <p>
                {this.state.feedback} <span>+{this.state.tapScore} points</span>
              </p>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default withRouter(Game);
