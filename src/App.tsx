import React from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import styled from "styled-components";
import "./App.css";

import Leaderboard from "./Leaderboard";
import Game from "./Game";
import Save from "./Save";

const StyledLink = styled(Link)`
  position: absolute;
  line-height: 1em;
  margin: 1em;
  padding: 0.25em 1em;
  border-radius: 3px;
  color: darkslategray;
  border: 2px solid darkslategray;
  text-decoration: none;
  font-style: italic;
`;

const Nav = styled.nav`
  background-color: #6d9998;
  height: calc(3.5em + 4px);
`;

const App: React.FC = () => {
  return (
    <Router>
      <Nav>
        <StyledLink to="/">Tap that Beat</StyledLink>
      </Nav>

      <Route exact path="/" component={Leaderboard} />
      <Route path="/game" component={Game} />
      <Route path="/save" component={Save} />
    </Router>
  );
};

export default App;
