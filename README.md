# Tap the Beat
This is a demo project made in React. This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app) for Node v10.15.3.

## The Game
The goal of the game is to tap a button at a certain beats per minute. Tapping at the correct pace earn maximum points, tapping at roughly the correct pace half points. The game contains three pages:
1. Leaderboard, this functions as the landing page and show the 5 highest scores
2. Game, here the actual game is played. After time runs out, redirects to Save
3. Save, show the final score and asks user to enter their name

## Running the app
Run `npm install` to install dependencies. After that, you can run the app by typing `npm start`. The app will be available at [http://localhost:3000](http://localhost:3000).
