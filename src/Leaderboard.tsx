import React from "react";
import axios from "axios";
import ScoreStorage from "./ScoreStorage";
import { Score } from "./Score";
import { RouteComponentProps, withRouter } from "react-router";
import { Link } from "react-router-dom";

import "./Leaderboard.css";

interface State {
  whatOthersSay: string;
}
interface Props extends RouteComponentProps {}

class Leaderboard extends React.Component<Props, State> {
  scoreList: JSX.Element[] = [];

  constructor(props: Props) {
    super(props);

    this.state = {
      whatOthersSay: ""
    };

    const scoreStorage: ScoreStorage = ScoreStorage.getInstance();
    const scores: Score[] = scoreStorage.getHighscores().slice(0, 5);
    const options: Intl.DateTimeFormatOptions = { year: "numeric", month: "long", day: "numeric", hour: "numeric", minute: "numeric", second: "numeric" };
    this.scoreList = scores.map((score, index) => (
      <tr key={index}>
        <td>{score.name}</td>
        <td>{score.score}</td>
        <td>{new Date(score.time).toLocaleDateString("en-US", options)}</td>
      </tr>
    ));
  }

  componentDidMount() {
    const url: string = "https://jsonplaceholder.typicode.com/comments";
    axios.get(url).then(res => {
      const comments = res.data;
      this.setState({ whatOthersSay: comments[0].body });
    });
  }

  render() {
    return (
      <div className="App">
        <h1>Tap that Beat</h1>

        <section className="Card">
          <header>
            <span>Highscores</span>
          </header>
          {this.scoreList.length > 0 ? (
            <table>
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Score</th>
                  <th>Time</th>
                </tr>
              </thead>
              <tbody>{this.scoreList}</tbody>
            </table>
          ) : (
            <p>No highscores yet! Press Start to play this game</p>
          )}
          <div className="StartButton">
            <Link to="/game">Start game</Link>
          </div>
        </section>

        <section className="Card">
          <div className="Testimonial">
            <img src="https://cdn.pixabay.com/photo/2014/05/22/16/57/man-351281_960_720.jpg" alt="Avatar" />
            <p>"{this.state.whatOthersSay}"</p>
            <p className="Name">
              <span>GrandMaster Game, 16 Jul 2019</span>
            </p>
          </div>
        </section>
      </div>
    );
  }
}

export default withRouter(Leaderboard);
