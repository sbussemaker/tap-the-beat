export interface Score {
  name: string;
  score: number;
  time: number;
}
