import { Score } from "./Score";

/**
 * ScoreStorage is a wrapper around window.localstorage for saving of the scores list
 */
class ScoreStorage {
  static instance: ScoreStorage | null;
  private readonly LOCAL_STORAGE_KEY = "scores";
  private scores: Score[];

  constructor() {
    let scoresString: string | null = window.localStorage.getItem(this.LOCAL_STORAGE_KEY);
    this.scores = scoresString != null ? JSON.parse(scoresString) : [];
  }

  static getInstance(): ScoreStorage {
    if (ScoreStorage.instance == null) {
      ScoreStorage.instance = new ScoreStorage();
    }

    return this.instance!;
  }

  getHighscores(): Score[] {
    this.scores.sort((a, b) => b.score - a.score); // sort by score, descending
    return this.scores;
  }

  addScore(newScore: Score): void {
    this.scores.push(newScore);
    window.localStorage.setItem(this.LOCAL_STORAGE_KEY, JSON.stringify(this.scores));
  }
}

export default ScoreStorage;
